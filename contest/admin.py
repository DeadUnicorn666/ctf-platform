from django.contrib import admin
from contest.models import Answer, Task, Achievement, Profile


admin.site.register(Answer)
admin.site.register(Task)
admin.site.register(Profile)
admin.site.register(Achievement)
