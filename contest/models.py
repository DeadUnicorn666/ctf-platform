from django.db import models
from django.contrib.auth.models import User


class Task(models.Model):
    name = models.CharField(max_length=100)
    score = models.IntegerField()
    correct_answer = models.CharField(max_length=100)
    category = models.CharField(max_length=30)


class Answer(models.Model):
    time = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.DO_NOTHING)
    task_category = models.CharField(max_length=30)
    text = models.CharField(max_length=1000)
    is_correct = models.BooleanField()
    score = models.IntegerField(default=0)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    score = models.IntegerField(default=0)


class Achievement(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=30)
