from django.shortcuts import render, redirect, render_to_response
from django.views.generic import FormView, TemplateView, CreateView, RedirectView
from django.views import View
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from contest.models import User, Task, Answer, Profile, Achievement
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from ctf_platform.settings import MAX_ATTEMPTS, TASKS_IN_CATEGORY, AMOUNT_OF_CATEGORIES, CATEGORY_BONUS, CATEGORIES
import json


class QuizPageView(View):
    template_name = "tasks.html"

    def get(self, request):
        tasks = Task.objects.values()
        if request.user.is_anonymous:
            solved_task_ids = []
            solving_task_ids = []
        else:
            correct_answers = Answer.objects.filter(user=request.user).filter(is_correct=True)
            solved_task_ids = [answer.task.id for answer in correct_answers]
            solving_task_ids = [task.id for task in Task.objects.filter(score=10)]
            solving_task_ids += [task_id + 1 for task_id in solved_task_ids if task_id % TASKS_IN_CATEGORY > 1]
            solving_task_ids = list(set(solving_task_ids) - set(solved_task_ids))
        result = {category: [] for category in CATEGORIES}
        for task in tasks:
            task_category = task['category']
            del task['category']
            del task['correct_answer']
            task['state'] = 'solved' if task['id'] in solved_task_ids \
                else 'solving' if task['id'] in solving_task_ids else 'unsolved'
            result[task_category].append(task)
        print(dict(result).items())
        return render(request, self.template_name, dict(data=dict(result).items()))


class BoardPageView(View):
    template_name = "scoreboard.html"

    def get(self, request):
        results = []
        users = User.objects.filter(is_superuser=False)
        for user in users:
            right_answers = Answer.objects.filter(user=user).filter(is_correct=True)
            results.append(dict(username=user.username,
                                score=sum([answer.task.score for answer in right_answers])))
        return render(request, self.template_name, dict(results=results))


class MainPageView(TemplateView):
    template_name = "main.html"


class BackgroundPageView(TemplateView):
    template_name = "background.html"


class TaskPageView(TemplateView):
    def get_template_names(self):
        return [f'tasks/{self.kwargs["task_id"]}.html']

    @method_decorator(login_required)
    def get(self, request, **kwargs):
        task_id = kwargs['task_id']
        is_allowed = self.is_allowed(request.user, task_id)
        solves = Answer.objects.filter(task_id=task_id).filter(user=request.user)
        if not is_allowed or solves.filter(is_correct=True) or len(solves) >= MAX_ATTEMPTS:
            return render(request, f'tasks/{task_id}.html', dict(blocked=True))
        return render(request, f'tasks/{task_id}.html', dict(blocked=False))

    @method_decorator(login_required)
    def post(self, request, **kwargs):
        task_id = kwargs['task_id']
        if not self.is_allowed(request.user, task_id):
            redirect(request.path)
        task = Task.objects.get(id=task_id)
        user_answers = Answer.objects.filter(user=request.user)
        solves = user_answers.filter(task_id=task_id)
        if solves.filter(is_correct=True) or len(solves) >= MAX_ATTEMPTS:
            return redirect(f'/task/{task_id}')
        ans_text = request.POST.get('answer')
        is_correct = task.correct_answer == ans_text
        answer = Answer(user=request.user, task=task, text=ans_text,
                        is_correct=is_correct, task_category=task.category, score=task.score if is_correct else 0)
        if is_correct:
            user = Profile.objects.get(user=request.user)
            user_solves = user_answers.filter(is_correct=True)
            if len(user_solves.filter(task_category=task.category)) == TASKS_IN_CATEGORY - 1:
                user.score += CATEGORY_BONUS
                if not Achievement.objects.filter(name=task.category):
                    user.score += CATEGORY_BONUS
                    Achievement(user=request.user, name=task.category).save()

            if len(user_solves.filter(score=task.score)) == AMOUNT_OF_CATEGORIES - 1:
                user.score += task.score
                if not Achievement.objects.filter(name=f'column_{task.score}'):
                    user.score += task.score
                    Achievement(user=request.user, name=f'column_{task.score}').save()
            user.score += task.score
            user.save()
        answer.save()

        if task.correct_answer == ans_text:
            return redirect('/tasks')
        return redirect(f'/task/{task_id}')

    @staticmethod
    def is_allowed(user, task_id):
        task = Task.objects.get(id=task_id)
        if task.score == 10:
            return True

        solves = Answer.objects.filter(user=user).filter(is_correct=True)
        if solves:
            last_solve = max(solves, key=lambda x: x.score)
        else:
            return False
        return task.score == last_solve.score + 10


class LoginPageView(FormView):
    template_name = "signin.html"
    form_class = AuthenticationForm
    success_url = "/"

    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(LoginPageView, self).form_valid(form)

    def form_invalid(self, form, *args, **kwargs):
        errors = json.loads(form.errors.as_json())
        error_messages = []
        for field in errors:
            for error in errors[field]:
                error_messages.append(error['message'])
        return render_to_response(self.template_name, context=dict(has_error=True, errors=error_messages,
                                                                   csrf_token=form.data['csrfmiddlewaretoken']))


class RegistrationPageView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('main')
    template_name = "signup.html"

    def form_valid(self, form, *args, **kwargs):
        self.object = form.save()
        login(self.request, self.object)
        profile = Profile(user=self.object)
        profile.save()
        return redirect(self.get_success_url())

    def form_invalid(self, form, *args, **kwargs):
        errors = json.loads(form.errors.as_json())
        error_messages = []
        for field in errors:
            for error in errors[field]:
                error_messages.append(error['message'])
        return render_to_response(self.template_name, context=dict(has_error=True, errors=error_messages,
                                                                   csrf_token=form.data['csrfmiddlewaretoken']))


class LogoutView(RedirectView):
    url = "/"

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)

