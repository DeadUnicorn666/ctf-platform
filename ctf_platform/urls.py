"""ctf_platform URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from contest import views
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.MainPageView.as_view(), name='main'),
    path('task/<int:task_id>', views.TaskPageView.as_view()),
    path('tasks/', views.QuizPageView.as_view(), name='tasks'),
    path('signin/', views.LoginPageView.as_view(), name='signin'),
    path('signup/', views.RegistrationPageView.as_view(), name='signup'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('scoreboard/', views.BoardPageView.as_view(), name='scoreboard'),
    path('background/', views.BackgroundPageView.as_view(), name='background'),
]
